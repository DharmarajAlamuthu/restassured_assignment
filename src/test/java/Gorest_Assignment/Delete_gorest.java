package Gorest_Assignment;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class Delete_gorest {
	@Test
	private void Delete_methods() {

		baseURI = "https://gorest.co.in";
		given().header("Authorization", "Bearer 36e58cf82540e431346d045cd08daa15cc94d59b74a2d9b6831eaa03f1a1eeb4")
				.when().delete("/public/v2/users/15989").then().statusCode(404);
	}

}
