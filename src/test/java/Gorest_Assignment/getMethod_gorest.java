package Gorest_Assignment;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class getMethod_gorest {
	
	@Test(priority = 1)
	public void GetMethod() {

		RequestSpecification http = RestAssured.given().header("Authorization",
				"Bearer 36e58cf82540e431346d045cd08daa15cc94d59b74a2d9b6831eaa03f1a1eeb4");
		Response response = RestAssured.get("https://gorest.co.in/public/v2/users");
		int statusCode = response.getStatusCode();
		System.out.println(statusCode);
		System.out.println(response.getBody().asString());
		Assert.assertEquals(statusCode, 200);
	}

	
	

}
