package Gorest_Assignment;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class postMethod_gorest {
	@Test(priority = 1)
	public void postMethods() {
		 JSONObject request = new JSONObject();
		  request.put("user_id", "204423");
		  request.put("title", "Selenium Java");
		  request.put("body", "Postman");
		  
		  
		  
		  baseURI="https://gorest.co.in";
		  given().header("Authorization","Bearer 36e58cf82540e431346d045cd08daa15cc94d59b74a2d9b6831eaa03f1a1eeb4")
		  .contentType("application/json")
		  .body(request.toJSONString()).when().post("/public/v2/posts")
		  .then().statusCode(201).log().all();

	
	}
}
