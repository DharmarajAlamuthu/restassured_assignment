package Gorest_Assignment;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;



public class putMethod_gorest {
	@Test
	public void puthMethod() {
		
		JSONObject request = new JSONObject();
		  request.put("user_id", "204426");
		  request.put("title", "Selenium Python");
		  request.put("body", "API RestAssured");
		  
		  
		  
		  baseURI="https://gorest.co.in";
		  given().header("Authorization","Bearer 36e58cf82540e431346d045cd08daa15cc94d59b74a2d9b6831eaa03f1a1eeb4")
		  .contentType("application/json")
		  .body(request.toJSONString()).when().put("/public/v2/posts/15989")
		  .then().statusCode(200).log().all();

	}

}
